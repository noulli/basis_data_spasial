-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 06, 2017 at 09:39 AM
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sig`
--

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE IF NOT EXISTS `pelanggan` (
  `kodePelanggan` int(10) NOT NULL,
  `namaPelanggan` varchar(100) NOT NULL,
  `alamatPelanggan` varchar(300) NOT NULL,
  `x` double NOT NULL,
  `y` double NOT NULL,
  `hapuskah` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pelanggan`
--

INSERT INTO `pelanggan` (`kodePelanggan`, `namaPelanggan`, `alamatPelanggan`, `x`, `y`, `hapuskah`) VALUES
(1, 'Toko Sembilan', 'Ruko Jati Kepuh, Jalan Raya Larangan. Larangan. Candi, Kabupaten Sidoarjo, Jawa Timur 61215', 112.7136989, -7.4686394, 0),
(2, 'CV. Tiga Dua', 'Jalan Simokerto 3 No.2, Simokerto, Kota SBY, Jawa Timur 60143', 112.7514248, -7.2394334, 0),
(3, 'Carefour Dukuh Kupang', 'Jl. Raya Dukuh Kupang No.126,Surabaya, Jawa Timur', 112.713457, -7.28747, 0),
(4, 'Toko Pojok', 'Jl. Krembangan Makam No.10, Surabaya, Jawa Timur', 112.7326551, -7.2365744, 0),
(5, 'Alfamart Tirta Raya', 'Ruko Tira Square jln. Tirta Raya, Surabaya, Jawa Timur', 112.7382599, -7.3518183, 0),
(6, 'Prima Freshmart Pucang Anom', 'Jl. Pucang Anom No.14, Pucang Sewu, Gubeng, Kota SBY, Jawa Timur 60283', 112.7525278, -7.28434, 0),
(7, 'Toko Hasanah', 'Jl. Sidosermo IV No.33, Sidosermo, Wonocolo, Kota SBY, Jawa Timur 60239', 112.7538995, -7.3103866, 0),
(8, 'Giant Extra Rajawali', 'Jalan Rajawali No.57, Kel. Perak Barat, Kec. Krembangan, Krembangan Sel., Krembangan, Kota SBY, Jawa Timur 60175', 112.7317745, -7.2351218, 0),
(9, 'UD. Sumber Karunia', 'Jl. Kenjeran No.482 B, Gading, Tambaksari, Kota SBY, Jawa Timur 60134', 112.7670626, -7.2416193, 0),
(10, 'INDOGROSIR Panjang Jiwo', 'Jl. Raya Prapen No.20, Panjang Jiwo, Tenggilis Mejoyo, Kota SBY, Jawa Timur 60299', 112.7573263, -7.3073937, 0),
(11, 'UD. Abyan Putra', ' Jl. Raya Berbek No.6, Berbek, Sidoarjo, Kabupaten Sidoarjo, Jawa Timur 61256', 112.7577213, -7.3496887, 0),
(12, 'Toko Primasari Pucang Anom', 'Pasar Pucang Anom, Jl. Pucang Anom, Kertajaya, Gubeng, Kota SBY, Jawa Timur 60283', 112.7312958, -7.2626051, 0),
(13, 'Toko Budi Jaya Tropodo', 'Jl. Tropodo Indah Blok A No. 1, Tropodo, Sidoarjo, Kabupaten Sidoarjo, Jawa Timur 61256', 112.7621158, -7.3602029, 0),
(14, 'Toko Adem Ayem', 'Jl. Kebraon II No.61, Kebraon, Karang Pilang, Kota SBY, Jawa Timur 60222', 112.6994153, -7.3356071, 0),
(15, 'Toko Suhartatik', 'Pasar LKMK, Jl. Semolowaru Tengah I, Semolowaru, Sukolilo, Kota SBY, Jawa Timur 60119', 112.7771803, -7.3024277, 0),
(16, 'Toko Bagus', 'Jl. Raya Menganti No.45, Jajar Tunggal, Wiyung, Kota SBY, Jawa Timur 60223', 112.7053625, -7.3128439, 0),
(17, 'Kepala Singa Gading Fajar Sidoarjo', 'JL Gading Fajar 2, Blok D7 No. 4, Sepande, Kec. Sidoarjo, Kabupaten Sidoarjo, Jawa Timur 61271', 112.7002491, -7.4653554, 0),
(18, 'Toko Damar Surya', 'No., Jl. H. Nur No.13, Sugihwaras, Kec. Sidoarjo, Kabupaten Sidoarjo, Jawa Timur 61271', 112.7039993, -7.4786207, 0),
(19, 'Roemah Snack Mekarsari', 'Perumahan Pondok Jati, Jl. Pd. Jati Blok C No.04, Jati, Kec. Sidoarjo, Kabupaten Sidoarjo, Jawa Timur 61252', 112.6965484, -7.4461094, 0),
(20, 'Ida Snack Dan Minuman', 'Jl. Raya Sukodono No.4a, Pekarungan, Sukodono, Kabupaten Sidoarjo, Jawa Timur 61258', 112.6282343, -7.4997889, 0),
(21, 'Sumber Perak', 'Jl. Perak Bar. No.169, Perak Bar., Krembangan, Kota SBY, Jawa Timur 60177', 112.7299533, -7.2216127, 0),
(22, 'Toko Mandiri', 'Jl. Nambangan No.118, Tanah Kali Kedinding, Kenjeran, Kota SBY, Jawa Timur 60129', 112.7144774, -7.2220313, 0),
(23, 'Toko Langgeng Makmur', 'Jalan Bangkingan Timur No.5, Sumur Welut, Lakarsantri, Kota SBY, Jawa Timur 60222', 112.6539774, -7.325646, 0),
(24, 'Supermarket bentar', 'Jl. Brigjen Katamso, Kepuhkiriman, Waru, Kabupaten Sidoarjo, Jawa Timur 61256', 112.7544498, -7.3502387, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sma`
--

CREATE TABLE IF NOT EXISTS `sma` (
  `npsn` varchar(10) NOT NULL,
  `sekolah` varchar(200) NOT NULL,
  `alamat` varchar(300) NOT NULL,
  `x` double NOT NULL,
  `y` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE IF NOT EXISTS `vendor` (
  `kodeVendor` int(10) NOT NULL,
  `namaVendor` varchar(100) NOT NULL,
  `alamatVendor` varchar(300) NOT NULL,
  `x` double NOT NULL,
  `y` double NOT NULL,
  `hapuskah` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor`
--

INSERT INTO `vendor` (`kodeVendor`, `namaVendor`, `alamatVendor`, `x`, `y`, `hapuskah`) VALUES
(1, 'CV Graha Mesin globalindo', 'Jalan Raya Kapi Sraba Ruko Kav.3 Blok 10B No 39, Mangliawan, Pakis, Malang, Jawa Timur 65154', 112.6713643, -7.9660424, 0),
(2, 'Duta Adpro', 'Jl. Ngagel Jaya Selatan III No.7, Pucang Sewu, Gubeng, Kota SBY, Jawa Timur 60283', 112.7527959, -7.2920753, 0),
(3, 'PT. Sentra tama Mandiri (ADVERTISING)', 'Pergudangan Kav. 16, Gang Tempura III, Gunung Anyar Tambak, Gunung Anyar, Gn. Anyar Tambak, Gn. Anyar, Kota SBY, Jawa Timur 60294', 112.7985357, -7.344473, 0),
(4, 'Prima Jaya Advertising', 'Gg. I- E Jl. Banyu Urip Wetan Blok, NO Blok NO No.1, Banyu Urip, Kec. Sawahan, Kota SBY, Jawa Timur 60254', 112.7251718, -7.2762246, 0),
(5, 'ATK Airlangga', 'Jl. Hasanuddin No.55, Celep, Kec. Sidoarjo, Kabupaten Sidoarjo, Jawa Timur 61215', 112.7181179, -7.462861, 0),
(6, 'Harmesindo', 'Jl. Kalidami No.26, Mojo, Gubeng, Kota SBY, Jawa Timur 60285', 112.7618185, -7.2752137, 0),
(7, 'PT 4Smarine', 'Perumahan Delta Mandala II No.87, Gedangan, Sidoarjo, Semambung, Sidoarjo, Kabupaten Sidoarjo, Jawa Timur 61254', 112.7416445, -7.3739766, 0),
(8, 'PT. Mesin Maksindo', 'Jl. Ngagel Raya No.77 M, Ngagel, Wonokromo, Kota SBY, Jawa Timur 60246', 112.7446144, -7.2813751, 0),
(9, 'Sapu jagad advertising', 'Jl. Ngagel Jaya Sel. No.98A, Ngagelrejo, Wonokromo, Kota SBY, Jawa Timur 60245', 112.7538278, -7.293914, 0),
(10, 'Aipel Computer', 'Jalan Raya Manyar 63 Ruko Manyar Mas 27 Blok C 60118, Menur Pumpungan, Sukolilo, Surabaya City, East Java 60118', 112.7608834, -7.2965102, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`kodePelanggan`);

--
-- Indexes for table `sma`
--
ALTER TABLE `sma`
  ADD PRIMARY KEY (`npsn`);

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
  ADD PRIMARY KEY (`kodeVendor`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pelanggan`
--
ALTER TABLE `pelanggan`
  MODIFY `kodePelanggan` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `vendor`
--
ALTER TABLE `vendor`
  MODIFY `kodeVendor` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
