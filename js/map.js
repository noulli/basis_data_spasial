(function(){
	window.onload=function(){;
	function initMap() {

        var uluru = {lat: -7.33, lng: 112.80};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 11,
          center: uluru
        });
		
		var markerVendor<?php echo $row['kodeVendor'] ?>=new google.maps.Marker({
          position:new google.maps.LatLng(<?php echo $row['y'] ?>, <?php echo $row['x'] ?>),
          map:map,
          title:'<?php echo $row['namaVendor'] ?>',
		  icon: 'img/vendor.png'
		});
		
		var markerPelanggan<?php echo $row['kodePelanggan'] ?>=new google.maps.Marker({
          position:new google.maps.LatLng(<?php echo $row['y'] ?>, <?php echo $row['x'] ?>),
          map:map,
          title:'<?php echo $row['namaPelanggan'] ?>',
		  icon: 'img/pelanggan.png'
		});
		
		var markerPusat=new google.maps.Marker({
          position:new google.maps.LatLng(-7.3558287,112.6997593),
          map:map,
          title:'Indofood Asahi',
		  icon: 'img/pusat.png'
		});
	}
}
})();

function tampilVendor() {
			var x = document.getElementById('inputVendor');
			if (x.style.visibility === 'collapse') {
				x.style.visibility = 'visible';
			} else {
				x.style.visibility = 'collapse';
			}
		};
		
function tampilPelanggan() {
			var y = document.getElementById('inputPelanggan');
			if (y.style.visibility === 'collapse') {
				y.style.visibility = 'visible';
			} else {
				y.style.visibility = 'collapse';
			}
		};