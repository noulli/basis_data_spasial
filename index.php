 <?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "sig";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


?>

<?php 	
	session_start();
//tekan edit Vendor
	if(isset($_GET['editedKodeVendor']))
	{
	 	$editedKodeVendor=$_GET['editedKodeVendor'];
		if(isset($_GET['statusHapus']))
		{
			if($_GET['statusHapus'])
			{ 
				$sql = "UPDATE `vendor` SET hapuskah=1 WHERE kodeVendor=".$editedKodeVendor;
				$conn->query($sql);
				$statusHapus=false; 
			}
		}
		else if(isset($_GET['statusEditVendor']))	
		{
			if($_GET['statusEditVendor'])
			{
				
					$sql = "SELECT kodeVendor, namaVendor, alamatVendor, x, y FROM `vendor` WHERE kodeVendor =".$editedKodeVendor;
					$result = $conn->query($sql);
					$editedRowVendor = $result->fetch_array();
					$_SESSION['editedKodeVendor']=$_GET['editedKodeVendor'];
			}
		}
	}
	
//tombol submit Vendor
	if(isset($_POST['btnsubmitVendor']))
	{
		$nama_vendor=$_POST['nmVendor'];
		$alamat_vendor=$_POST['almtVendor'];
		$lngX=$_POST['xVendor'];
		$ltdY=$_POST['yVendor'];
	
		if(isset ($_GET['statusEditVendor']))
		{
			$editedKodeVendor=$_SESSION['editedKodeVendor'];
			$sql = "UPDATE vendor SET namaVendor ='$nama_vendor' , alamatVendor='$alamat_vendor', x=$lngX, y=$ltdY
					WHERE kodeVendor =$editedKodeVendor";
			$conn->query($sql);
			$statusEditVendor=false;
			unset($_GET['statusEditVendor']);
			header("Location:index.php");
		}
		else
		{
			$sql = "INSERT INTO vendor(namaVendor,alamatVendor,x,y) values 
			('$nama_vendor', '$alamat_vendor','$lngX', '$ltdY')";
			$conn->query($sql);
		}	
	}

//tekan edit Pelanggan
	if(isset($_GET['editedKodePelanggan']))
	{
	 	$editedKodePelanggan=$_GET['editedKodePelanggan'];
		if(isset($_GET['statusHapus']))
		{
			if($_GET['statusHapus'])
			{ 
				$sql = "UPDATE `pelanggan` SET hapuskah=1 WHERE kodePelanggan=".$editedKodePelanggan;
				$conn->query($sql);
				$statusHapus=false; 
			}
		}
		else if(isset($_GET['statusEditPelanggan']))	
		{
			if($_GET['statusEditPelanggan'])
			{
				
					$sql = "SELECT kodePelanggan, namaPelanggan, alamatPelanggan, x, y FROM `pelanggan` WHERE kodePelanggan =".$editedKodePelanggan;
					$result = $conn->query($sql);
					$editedRowPelanggan = $result->fetch_array();
					$_SESSION['editedKodePelanggan']=$_GET['editedKodePelanggan'];
			}
		}
	}
	
//tombol submit Pelanggan
	if(isset($_POST['btnsubmitPelanggan']))
	{
		$nama_pelanggan=$_POST['nmPelanggan'];
		$alamat_pelanggan=$_POST['almtPelanggan'];
		$lngX=$_POST['xPelanggan'];
		$ltdY=$_POST['yPelanggan'];
	
		if(isset ($_GET['statusEditPelanggan']))
		{
			$editedKodePelanggan=$_SESSION['editedKodePelanggan'];
			$sql = "UPDATE pelanggan SET namaPelanggan ='$nama_pelanggan' , alamatPelanggan='$alamat_pelanggan', x=$lngX, y=$ltdY
					WHERE kodePelanggan =$editedKodePelanggan";
			$conn->query($sql);
			$statusEditPelanggan=false;
			unset($_GET['statusEditPelanggan']);
			header("Location:index.php");
		}
		else
		{
			$sql = "INSERT INTO pelanggan(namaPelanggan,alamatPelanggan,x,y) values 
			('$nama_pelanggan', '$alamat_pelanggan','$lngX', '$ltdY')";
			$conn->query($sql);
		}	
	}
 ?>

<!DOCTYPE html>
<html>
  <head>
	<title>Peta Vendor & Pelanggan</title>
    <style>
		table{margin-top: 5px; width:90%;}
		fieldset{width: 85%; margin-top: 3px;}
		div{display:inline-block}
		.atas{width: 200px;}
		.tengah{display:inherit; width: 200px;}		
		.bawah{display: inline-block; width: 200px;}
		#inputVendor{width:45%}
		#inputPelanggan{width:45%}
		#info{width: 100%;}
       #map {
        height: 450px;
        width: 100%;
       }
    </style>
	<script type="text/javascript" src="js/jquery.js"></script>
	<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBou74hiiEttnG_hbUrb9VPXqy7mXhn5i4&callback=initMap">
    </script>
  </head>
  <body>
<!--Tombol Vendor-->
	<button onclick="tampilVendor()"()>Vendor</button>
	<script>
		function tampilVendor() {
			var x = document.getElementById('inputVendor');
			if (x.style.display === 'none') {
				x.style.display = 'inline-block';
			} else {
				x.style.display = 'none';
			}
		}
	</script>
<!--Tombol Pelanggan-->
	<button onclick="tampilPelanggan()">Pelanggan</button>
	<script>
		function tampilPelanggan() {
			var y = document.getElementById('inputPelanggan');
			if (y.style.display === 'none') {
				y.style.display = 'inline-block';
			} else {
				y.style.display = 'none';
			}
		}
	</script>
    <h2 align="center"><a href="index.php">PT. Indofood Asahi Sukses Beverage</a></h2>
    <div id="map"></div>
    <script>
      function initMap() {
		var infoWindow;
        var latlng = {lat: -7.33, lng: 112.80};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 11,
          center: latlng
        });

<?php

$sql = "SELECT * FROM vendor";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) { ?>
//marker untuk Vendor
        var markerVendor<?php echo $row['kodeVendor'] ?>=new google.maps.Marker({
          position:new google.maps.LatLng(<?php echo $row['y'] ?>, <?php echo $row['x'] ?>),
          map:map,
          title:'<?php echo $row['namaVendor'] ?>',
		  icon: 'img/vendor.png'
    });
//infoWindow Vendor
		google.maps.event.addListener(markerVendor<?php echo $row['kodeVendor'] ?>,'click', function(){
			if(!infoWindow){
				infoWindow=new google.maps.InfoWindow();
			}
			var content='<div id="info">'+
				'<img src="img/vendor.png" />'+
				'<h3><?php echo $row['namaVendor'] ?></h3>'+
				'<p><?php echo $row['alamatVendor'] ?></p>'+
				'</div>';
			infoWindow.setContent(content);
			infoWindow.open(map, markerVendor<?php echo $row['kodeVendor'] ?>);
		});
<?php }} ?>

<?php

$sql = "SELECT * FROM pelanggan";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) { ?>
//marker untuk Pelanggan
        var markerPelanggan<?php echo $row['kodePelanggan'] ?>=new google.maps.Marker({
          position:new google.maps.LatLng(<?php echo $row['y'] ?>, <?php echo $row['x'] ?>),
          map:map,
          title:'<?php echo $row['namaPelanggan'] ?>',
		  icon: 'img/pelanggan.png'
    });
//infoWindow Pelanggan
		google.maps.event.addListener(markerPelanggan<?php echo $row['kodePelanggan'] ?>,'click', function(){
			if(!infoWindow){
				infoWindow=new google.maps.InfoWindow();
			}
			var content='<div id="info">'+
				'<img src="img/pelanggan.png" />'+
				'<h3><?php echo $row['namaPelanggan'] ?></h3>'+
				'<p><?php echo $row['alamatPelanggan'] ?></p>'+
				'</div>';
			infoWindow.setContent(content);
			infoWindow.open(map, markerPelanggan<?php echo $row['kodePelanggan'] ?>);
		});
	
<?php }} ?>
//marker untuk Indofood Asahi
	  var markerPusat=new google.maps.Marker({
          position:new google.maps.LatLng(-7.3558287,112.6997593),
          map:map,
          title:'Indofood Asahi',
		  icon: 'img/pusat.png'
    });
//infoWindow Indofood
		google.maps.event.addListener(markerPusat,'click', function(){
			if(!infoWindow){
				infoWindow=new google.maps.InfoWindow();
			}
			var content='<div id="info">'+
				'<img src="img/indofood.png" />'+
				'<h3>PT. Indofood Asahi Sukses Beverage</h3>'+
				'<p>Jl. Tambak Sawah No.20, Tambakrejo, Waru, Kabupaten Sidoarjo, Jawa Timur 61256</p>'+
				'</div>';
			infoWindow.setContent(content);
			infoWindow.open(map, markerPusat<?php echo $row['kodeVendor'] ?>);
		});
  }
    </script>
	

<!--tambah dan edit Vendor-->
	<div id="inputVendor" style="display:none;">
		<form method="POST" action="">
			<fieldset>
				<h4>Tambah/Update Vendor</h4>
				<div class="atas">
				<label>Nama Vendor</label>
				<input type="text" name="nmVendor" required="" placeholder="PT. X" value="<?php if(isset($_GET['statusEditVendor'])) echo $editedRowVendor['namaVendor'] ?>">
				</div>
				<div class="tengah">
				<label>Alamat</label>
				<input type="text" name="almtVendor"  required="" placeholder="Jl. X" value="<?php if(isset($_GET['statusEditVendor'])) echo $editedRowVendor['alamatVendor'] ?>">
				</div>
				<div class="bawah">
				<label>Longitude (x)</label>
				<input type="text" name="xVendor" required="" placeholder="123.5678" value="<?php if(isset($_GET['statusEditVendor'])) echo $editedRowVendor['x'] ?>">
				</div>
				<div class="bawah">
				<label>Latitude (y)</label>
				<input type="text" name="yVendor" required="" placeholder="-1.2345" value="<?php if(isset($_GET['statusEditVendor'])) echo $editedRowVendor['y'] ?>">
				</div><br>
				<input type="submit" name="btnsubmitVendor" value="submit">
			</fieldset>
		</form>
<!--tabel vendor-->	
		<table border="1">
			<thead>
				<th>Kode Vendor</th>
				<th>Nama Vendor</th>
				<th>Longitude (x)</th>
				<th>Latitude (y)</th>
				<th>EDIT/HAPUS</th>
			</thead>
			<tbody>
			<?php 
				$sql = "select * from vendor where hapuskah=0";
				$hasil=$conn->query($sql);
				if($hasil)
				{
					while($row=$hasil->fetch_array())
					{
						if($row['hapuskah'] == 0)
						{
							echo "<tr>";
							echo "<td>".$row['kodeVendor']."</td>";
							echo "<td>".$row['namaVendor']."</td>";
							echo "<td>".$row['x']."</td>";
							echo "<td>".$row['y']."</td>";
							echo "<td><a href='index.php?editedKodeVendor=".$row['kodeVendor']."&statusEditVendor=true'>Edit</a> | 
									<a href='index.php?editedKodeVendor=".$row['kodeVendor']."&statusHapus=true'>Hapus</a></td>";
							echo "</tr>";
						}
					}
				}
			 ?>
			</tbody>
		</table>
	</div>
	

<!--tambah dan edit Pelanggan-->
	<div id="inputPelanggan" style="display:none;">
		<form method="POST" action="">
			<fieldset>
				<h4>Tambah/Update Pelanggan</h4>
				<div class="atas">
				<label>Nama Pelanggan</label>
				<input type="text" name="nmPelanggan" required="" placeholder="PT. X" value="<?php if(isset($_GET['statusEditPelanggan'])) echo $editedRowPelanggan['namaPelanggan'] ?>">
				</div>
				<div class="tengah">
				<label>Alamat</label>
				<input type="text" name="almtPelanggan" required="" placeholder="Jl. X" value="<?php if(isset($_GET['statusEditPelanggan'])) echo $editedRowPelanggan['alamatPelanggan'] ?>">
				</div>
				<div class="bawah">
				<label>Longitude (x)</label>
				<input type="text" name="xPelanggan" required="" placeholder="123.4567" value="<?php if(isset($_GET['statusEditPelanggan'])) echo $editedRowPelanggan['x'] ?>">
				</div>
				<div class="bawah">
				<label>Latitude (y)</label>
				<input type="text" name="yPelanggan" required="" placeholder="-1.2345" value="<?php if(isset($_GET['statusEditPelanggan'])) echo $editedRowPelanggan['y'] ?>">
				</div><br>
				<input type="submit" name="btnsubmitPelanggan" value="submit">
			</fieldset>
		</form>
<!--tabel Pelanggan-->	
		<table border="1">
			<thead>
				<th>Kode Pelanggan</th>
				<th>Nama Pelanggan</th>
				<th>Longitude (x)</th>
				<th>Latitude (y)</th>
				<th>EDIT/HAPUS</th>
			</thead>
			<tbody>
			<?php 
				$sql = "select * from pelanggan where hapuskah=0";
				$hasil=$conn->query($sql);
				if($hasil)
				{
					while($row=$hasil->fetch_array())
					{
						if($row['hapuskah'] == 0)
						{
							echo "<tr>";
							echo "<td>".$row['kodePelanggan']."</td>";
							echo "<td>".$row['namaPelanggan']."</td>";
							echo "<td>".$row['x']."</td>";
							echo "<td>".$row['y']."</td>";
							echo "<td><a href='index.php?editedKodePelanggan=".$row['kodePelanggan']."&statusEditPelanggan=true'>Edit</a> | 
									<a href='index.php?editedKodePelanggan=".$row['kodePelanggan']."&statusHapus=true'>Hapus</a></td>";
							echo "</tr>";
						}
					}
				}
			 ?>
			</tbody>
		</table>
	</div>
  </body>
</html>

